package ru.kuzin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.dto.model.UserDTO;
import ru.kuzin.tm.enumerated.Role;

public interface IUserRepository extends IRepository<UserDTO> {

    @Nullable
    UserDTO create(@NotNull IPropertyService propertyService, @NotNull String login, @NotNull String password);

    @Nullable
    UserDTO create(@NotNull IPropertyService propertyService, @NotNull String login, @NotNull String password, @Nullable String email);

    @Nullable
    UserDTO create(@NotNull IPropertyService propertyService, @NotNull String login, @NotNull String password, @NotNull Role role);

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    Boolean isLoginExist(@NotNull String login);

    Boolean isEmailExist(@NotNull String email);

}