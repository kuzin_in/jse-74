package ru.kuzin.tm.exception;

public final class IdEmptyException extends AbstractException {

    public IdEmptyException() {
        super("Error! Id is empty...");
    }

}